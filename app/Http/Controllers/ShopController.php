<?php

namespace App\Http\Controllers;

use App\Eloquent\Catalog;
use App\Eloquent\Product;
use DB;

class ShopController extends Controller //chuyen de viet nhung cai hien thi ngoai front end cua anh hUng
{
    //

    public function index()
    {
        $products = Product::paginate(6);
        $categories = Catalog::all();
        $relate_products = Product::paginate(4);

        $list_recommends = [];

        $calalogs = Catalog::get();

        foreach ($calalogs as $catalog) {
            $recommend_products = $catalog->products()->orderBy('view', 'desc')->take(3)->get();
            $list_recommends[] = $recommend_products;
        }


//        dd($arr);

        return view('shop.home')->with([
            'products' => $products,
            'categories' => $categories,
            'relate_products' => $relate_products,
            'recommend_products' => $list_recommends
        ]);
    }

    public function showdetail($id)
    {
        $products = Product::all();
        $categories = Catalog::all();
        $product_detail = Product::find($id);

        return view('shop.product-detail')->with([
            'products' => $products,
            'categories' => $categories,
            'product_detail' => $product_detail,
            ]);
    }
}
