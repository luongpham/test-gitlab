<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ProductProductPhoto extends Model
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'product_product_photos';

    /**
     * Timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relation Products Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

    /**
     * Relation Product_image Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product_image(){
        return $this->hasOne(ProductImages::class,'product_photo_id', 'product_photo_id');
    }

    /**
     * Relation Product_images Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product_images(){
        return $this->hasMany(ProductImages::class,'product_photo_id', 'product_photo_id');
    }

    public function getAllImage(){
        $imgs = DB::table('product_photos')->select('product_photos.thumbnail_photo_link')
            ->join('product_product_photos', 'product_photos.product_photo_id', '=', 'product_product_photos.product_photo_id')
            ->join('products', 'product_product_photos.product_id', '=', 'products.id')
            ->get();
         $imgs[0]->thumbnail_photo_link;
    }

}
