<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    /**
     * PrimaryKey
     *
     * @var string
     */
    protected $primaryKey = 'product_photo_id';

    /**
     * Table
     *
     * @var string
     */
    protected $table = 'product_photos';

    /**
     * Fillable
     *
     * @var array
     */
    protected $fillable = [
        'product_photo_id',
        'thumbnail_photo_link',
        'thumbnail_photo_name'
    ];

    /**
     * Timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relation Product Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(ProductProductPhoto::class,'product_photo_id');
    }
}
    