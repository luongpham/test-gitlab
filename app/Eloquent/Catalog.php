<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    /**
     * Table
     *
     * @var string
     */
    protected $table='catalogs';

    /**
     * Fillable
     *
     * @var array
     */
    protected $fillable=['catalog_name'];

    /**
     * Primary Key
     *
     * @var string
     */
    protected $primaryKey = 'catalog_id';

    /**
     * Timestamps
     *
     * @var bool
     */
    public $timestamps=false;

    /**
     * Relation Products Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products(){
        return $this->hasMany(Product::class,'catalog_id');
    }

    /**
     * Get Feature Products
     *
     * @return mixed
     */
    public function getFeatureProducts(){
        return $this->products()->take(4)->get();
    }
}
